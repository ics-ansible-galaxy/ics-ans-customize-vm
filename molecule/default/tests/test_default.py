import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('customize_vm')


def test_sssd(host):
    service = host.service("sssd")
    assert service.is_running
    assert service.is_enabled
