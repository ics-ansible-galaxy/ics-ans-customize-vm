# ics-ans-customize-vm

Ansible playbook to cutomize users created VMs.

## Requirements

- ansible >= 2.4
- molecule >= 2.6

## License

BSD 2-clause
